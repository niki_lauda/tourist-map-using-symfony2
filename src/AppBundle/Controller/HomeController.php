<?php

namespace AppBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Document\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\SessionData;
class HomeController extends Controller{
    /**
     * @Route("/")
     */
    public function indexAction(Request $request) {
        return $this->redirectToRoute("home");
    }

    function b($a, $b) {
        $ad = $a->getAddTime();
        $bd = $b->getAddTime();

        if ($ad == $bd) {
            return 0;
        }

        return $ad < $bd ? 1 : -1;
    }
    /**
     * @Route("/home", name="home")
     */
    public function homeAction(Request $request){
        $news = $this->getRepository('News')->findAll();
        $places = $this->getRepository('Place')->findAll();
        $events = $this->getRepository('Event')->findAll();
        uasort($news, array('AppBundle\Controller\HomeController', 'b'));
        uasort($places, array('AppBundle\Controller\HomeController', 'b'));
        uasort($events, array('AppBundle\Controller\HomeController', 'b'));
        $news = array_slice($news, 0, 4);
        $places = array_slice($places, 0, 4);
        $events = array_slice($events, 0, 4);
        
        $form = $this->buildSearchForm();
        return $this->render('home/index.twig', 
          array('userName'=> SessionData::sessionData($request), 
            'form' => $this->buildSearchForm()->createView(),
            'news'=> $news, 'places'=>$places, 'events'=>$events));
    }
    /**
     * @Route("/searchResults={page}", name="searchResults",defaults = {"page": 1}, requirements = {"page" : "\d+"} )
     */
    public function findAction(Request $request, $page) {
        $form = $this->buildSearchForm();
        $form->handleRequest($request);
        if ($form->isValid()) {
            $all_items = null;
            if($form["city"]->getData()){
                $all_items = $this->getRepository('Place')->findBy(
                  array('country' => $form["country"]->getData(), 'city' => $form["city"]->getData()));
            }
            else{
                $all_items = $this->getRepository('Place')->findByCountry($form["country"]->getData());
            }
            $numbers = 1;
            if(count($all_items) / 10 > 2){
                $numbers = range(1, count($all_items) / 10);
                $all_items = array_slice($all_items, ($page - 1) * 10, 10);
            }
            return $this->render('home/searchResults.twig',
              array('userName'=> SessionData::sessionData($request),
                'form' => $form->createView(),'places'=>$all_items,  'pages_count' => $numbers));
        }
        $this->redirectInTime(10);
        return $this->render('templates/message.twig',
          array('message'=> 'Sorry, there is not something found, that matches your interests...'));
    }

        public function buildSearchForm() {
            return $this->createFormBuilder()
              ->setAction($this->generateUrl('searchResults'))
              ->add('country', 'text', array('label'=>'Country', 'attr'=> array('placeholder'=>'Enter country')))
              ->add('city', 'text', array('required' => false,'label'=> 'City', 'attr'=> array('placeholder'=>'Leave empty to view all cities')))
              ->add('search', 'submit', array('label' => 'Search'))
              ->getForm();
        }



        /**
     * @return mixed
     */
    public function getRepository($rep) {
        $repository = $this->get('doctrine_mongodb')
          ->getManager()
          ->getRepository('AppBundle:'.$rep);
        return $repository;
    }
}

