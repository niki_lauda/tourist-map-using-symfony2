<?php

  namespace AppBundle\Controller;

  use Symfony\Bundle\FrameworkBundle\Controller\Controller;
  use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
  use Symfony\Component\HttpFoundation\Request;
  use AppBundle\SessionData;
  use AppBundle\Document\Place;
  use AppBundle\Document\Comment;

  class PlacesController extends Controller {

    /**
     * @Route("/places={page}", name="places",defaults = {"page": 1}, requirements = {"page" : "\d+"} )
     */
    public function showAllAction(Request $request, $page) {
      $all_items = $this->getPlacesRepository()->findAll();
      uasort($all_items,
        function($a, $b) {
          $ad = $a->getAddTime();
          $bd = $b->getAddTime();

          if ($ad == $bd) {
            return 0;
          }

          return $ad < $bd ? 1 : -1;
        });
      $numbers = range(1, count($all_items) / 10);
      $items = array_slice($all_items, ($page - 1) * 10, 10);
      return $this->render('categories/places.twig',
        array('items' => $items, 'userName' => SessionData::sessionData($request),
          'pages_count' => $numbers));
    }


    /**
     * @Route("/place={id}", name="place")
     */
    public function showOneAction(Request $request, $id) {
      $dm = $this->get('doctrine_mongodb')
        ->getManager();
      $item = $dm->getRepository('AppBundle:Place')->findOneByid($id);
      $comment = new Comment();
      $commentForm = $this->getCommentForm($comment);
      $commentForm->handleRequest($request);
      
      if ($request->getMethod() == 'POST' and $commentForm->isValid()) {
        $comment->setRate($request->request->get('rate'));
        if ($request->getSession()->has('sessionData')) {
          $comment->setAutor($dm->getRepository('AppBundle:User')->findOneByemail(
            $request->getSession()->get('sessionData')->getEmail()
          ));
        }
        $date = new \DateTime();
        $comment->setAddTime($date);
        $item->addComment($comment);
        $dm->flush();
        unset($comment);
        unset($commentForm);
        $comment = new Comment();
        $commentForm = $this->getCommentForm($comment);
      }
      
      return $this->render('categories/place.twig',
        array('item' => $item, 'userName' => SessionData::sessionData($request),
          'form' => $commentForm->createView(), 'cord_a' => $item->getCoordX(), 'cord_b'=>$item->getCoordY()));
    }

    public function saveToDBComment($comment){
    }


    /**
     * @Route("/addplace", name="addplace")
     */
    public function addPlaceAction(Request $request) {
      if (!$request->getSession()->has('sessionData')) {
        return $this->redirectToRoute("places");
      }
      $place = new Place();
      $form = $this->getPlaceForm($place);
      $form->handleRequest($request);


      if ($form->isValid()) {
        $file = $place->getFotoName();
        $fileName = md5(uniqid()) . '.' . $file->guessExtension();
        $brochuresDir = 'img/img-places';
        $file->move($brochuresDir, $fileName);
        $place->setFotoName($fileName);
        $this->saveToDBPlace($place);
        return $this->redirect('places');
      }

      return $this->render("categories/addplace.twig",
        array('form' => $form->createView(), 'userName' => SessionData::sessionData($request)));
    }


    public function getPlaceForm($news) {
      return $this->createFormBuilder($news)
        ->add('name', 'text', array('error_bubbling' => true,
          'label' => ' Name', 'attr' => array('name' => 'name')))
        ->add('englishName', 'text', array('required' => false, 'error_bubbling' => true,
          'label' => 'English Name', 'attr' => array('name' => 'englishName')))
        ->add('country', 'text', array('error_bubbling' => true,
          'label' => 'Country', 'attr' => array('name' => 'country')))
        ->add('city', 'text', array('error_bubbling' => true,
          'label' => 'City', 'attr' => array('name' => 'city')))
        ->add('region', 'text', array('required' => false,'error_bubbling' => true,
          'label' => 'Region', 'attr' => array('name' => 'region')))
        ->add('description', 'textarea', array('error_bubbling' => true,
          'label' => 'Description', 'attr' => array('name' => 'description')))
        ->add('coord_x', 'number', array('error_bubbling' => true,
          'label' => 'X Coordinate:', 'attr' => array('name' => 'coord_x')))
        ->add('coord_y', 'number', array('label' => 'Y Coordinate:', 'attr' => array('name' => 'coord_y')))
        ->add('telephoneNumber', 'text', array('required' => false,'error_bubbling' => true,
          'label' => 'TelephoneNumber', 'attr' => array('name' => 'telephoneNumber')))
        ->add('webPage', 'text', array('required' => false,'error_bubbling' => true,
          'label' => 'Web-Page', 'attr' => array('name' => 'webPage')))
        ->add('email', 'text', array('required' => false,'error_bubbling' => true,
          'label' => 'Contact E-mail', 'attr' => array('name' => 'email')))
        ->add('fotoName', 'file', array('label' => 'Image', 'attr'=>array('style'=>'display: none','onclick'=>'fill')))
        ->add('button', 'button', array('label' => 'Browse', 
          'attr'=>array("type"=>"button", 'class'=>"pure-button", 'value'=>"Browse...",
               'onclick'=>"document.getElementById('form_fotoName').click();")))
        ->add('save', 'submit', array('label' => 'Add place', 'attr' => array('class'=>'pure-button b', 'name' => 'save')))
        ->getForm();
    }


    public function saveToDBPlace($place) {
      $date = new \MongoDate();
      $place->setAddTime($date);
      $dm = $this->get('doctrine_mongodb')->getManager();
      $dm->persist($place);
      $dm->flush();
    }
    


    public function getCommentForm($comment) {
      return $this->createFormBuilder($comment)
        ->add('impressions', 'textarea', array('error_bubbling' => true,
          'label' => 'Impressions', 'attr' => array()))
        ->add('advantages', 'textarea', array('required' => false, 'error_bubbling' => true,
          'label' => 'Advantages', 'attr' => array()))
        ->add('disadvantages', 'textarea', array('required' => false, 'error_bubbling' => true,
          'label' => 'Disadvantages', 'attr' => array()))
        ->add('save', 'submit', array('label' => 'Post comment'))
        ->getForm();
    }


    public function getPlacesRepository() {
      $repository = $this->get('doctrine_mongodb')
        ->getManager()
        ->getRepository('AppBundle:Place');
      return $repository;
    }
  }