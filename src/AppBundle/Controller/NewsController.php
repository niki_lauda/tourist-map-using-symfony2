<?php

  namespace AppBundle\Controller;
  use Symfony\Bundle\FrameworkBundle\Controller\Controller;
  use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
  use AppBundle\SessionData;
  use AppBundle\Document\News;
  use Symfony\Component\HttpFoundation\Request;
  use AppBundle\Document\Comment;



  class NewsController extends Controller {
    

    
    /**
     * @Route("/news={page}", name="news", defaults = {"page": 1}, requirements = {"page" : "\d+"})
     */
    public function showAction(Request $request, $page){

      $all_items = $this->getNewsRepository()->findAll();
      uasort($all_items,   
        function($a, $b) {
          $ad = $a->getAddTime();
          $bd = $b->getAddTime();

          if ($ad == $bd) {
            return 0;
          }

          return $ad < $bd ? 1 : -1;
      });
      $numbers = 1;
      $items = $all_items;
      $news = new News();
      if(count($all_items) > 10){
        $numbers = range(1, count($all_items)/10);
        $items = array_slice($all_items, ($page-1)*10, 10);
      }
      return $this->render("categories/news.twig",
        array('posts'=> $items, 'pages_count' => $numbers, 'userName'=> SessionData::sessionData($request)));
    }
    /**
     * @Route("/newitem={id}", name="newitem")
     */
    public function showOneAction(Request $request, $id){
      $dm = $this->get('doctrine_mongodb')
        ->getManager();
      $item = $dm->getRepository('AppBundle:News')->findOneByid($id);
      $comment = new Comment();
      $commentForm = $this->getCommentForm($comment);
      $commentForm->handleRequest($request);

      if ($request->getMethod() == 'POST' and $commentForm->isValid()) {
        $comment->setRate($request->request->get('rate'));
        if ($request->getSession()->has('sessionData')) {
          $comment->setAutor($dm->getRepository('AppBundle:User')->findOneByemail(
            $request->getSession()->get('sessionData')->getEmail()
          ));
        }
        $date = new \DateTime();
        $comment->setAddTime($date);
        $item->addComment($comment);
        $dm->flush();
        unset($comment);
        unset($commentForm);
        $comment = new Comment();
        $commentForm = $this->getCommentForm($comment);
      }
      return $this->render('categories/newitem.twig',
        array('item'=>$item, 'userName'=> SessionData::sessionData($request),
          'form'=> $commentForm->createView()));
    }

    /**
     * @Route("/addnews", name="addnews");
     */
    public function addNewsAction(Request $request){
      if(!$request->getSession()->has('sessionData')){
        return $this->redirectToRoute("news");
      }
      $news = new News();
      $form = $this->getNewsForm($news);
      $form->handleRequest($request);


      if ($form->isValid()) {
        $file = $news->getImageName();
        $fileName = md5(uniqid()).'.'.$file->guessExtension();
        $brochuresDir ='img/news';
        $file->move($brochuresDir, $fileName);
        $news->setImageName($fileName);
        $this->saveToDB($news);
        return $this->redirect('news');
      }
      
      return $this->render("categories/addnew.twig",
        array('form'=> $form->createView(), 'userName'=> SessionData::sessionData($request)));
    }

    public function getNewsForm($news){
      return $this->createFormBuilder($news)
        ->add('title', 'text', array('error_bubbling' => true,
          'label'=>'Title'))
        ->add('short', 'textarea', array('error_bubbling' => true,
          'label'=>'Short Description'))
        ->add('full', 'textarea', array('error_bubbling' => true,
          'label'=>'Content'))
        ->add('imageName', 'file', array('label' => 'Image'))
        ->add('save', 'submit', array('label' => 'Post'))
        ->getForm();
    }

    public function saveToDB($news) {
      $date = new \MongoDate();
      $news->setAddTime($date);
      $dm = $this->get('doctrine_mongodb')->getManager();
      $dm->persist($news);
      $dm->flush();
    }

    public function getNewsRepository() {
      return $repository = $this->get('doctrine_mongodb')
        ->getManager()
        ->getRepository('AppBundle:News');
    }
    
    public function getCommentForm($comment) {
      return $this->createFormBuilder($comment)
        ->add('impressions', 'textarea', array('error_bubbling' => true,
          'label' => 'Impressions', 'attr' => array()))
        ->add('advantages', 'textarea', array('required' => false, 'error_bubbling' => true,
          'label' => 'Advantages', 'attr' => array()))
        ->add('disadvantages', 'textarea', array('required' => false, 'error_bubbling' => true,
          'label' => 'Disadvantages', 'attr' => array()))
        ->add('save', 'submit', array('label' => 'Post comment'))
        ->getForm();
    }

  }