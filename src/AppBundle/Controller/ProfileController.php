<?php

  namespace AppBundle\Controller;
  use Symfony\Bundle\FrameworkBundle\Controller\Controller;
  use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
  use Symfony\Component\HttpFoundation\Request;
  use AppBundle\SessionData;
  class ProfileController extends Controller {

    /**
     * @Route("/profile", name="profile")
     */
    public function profileViewAction(Request $request) {
      $data = SessionData::sessionData($request);
      if(!$data)  {
        return $this->redirectToRoute("home");
      }
      $manager = $this->get("doctrine_mongodb")->getManager();
      $user = $manager->getRepository('AppBundle:User')->findOneByemail(
        $data->getEmail());
      return $this->render('personal/profile.twig',
        array('userName'=> SessionData::sessionData($request),
          'user'=> $user));
    }
  }