<?php

  namespace AppBundle\Controller;
  use Symfony\Bundle\FrameworkBundle\Controller\Controller;
  use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
  use AppBundle\SessionData;
  use AppBundle\Document\Event;
  use AppBundle\Document\Comment;
  use Symfony\Component\HttpFoundation\Request;
  class RecommendController extends Controller{
    /**
   * @Route("/events={page}", name="events", defaults = {"page": 1}, requirements = {"page" : "\d+"})
   */
    public function showAction(Request $request, $page){
    $all_items = $this->getEventsRepository()->findAll();
      uasort($all_items,
        function($a, $b) {
          $ad = $a->getAddTime();
          $bd = $b->getAddTime();

          if ($ad == $bd) {
            return 0;
          }

          return $ad < $bd ? 1 : -1;
        });
    $numbers = 1;
    $items = $all_items;
    if(count($all_items) > 10){
      $numbers = range(1, count($all_items)/10);
      $items = array_slice($all_items, ($page-1)*10, 10);
    }
    return $this->render("categories/events.twig",
      array('posts'=> $items, 'pages_count' => $numbers, 'userName'=> SessionData::sessionData($request)));
  }
    /**
     * @Route("/event={id}", name="event")
     */
    public function showOneAction(Request $request, $id){
      $dm = $this->get('doctrine_mongodb')
        ->getManager();
      $item = $dm->getRepository('AppBundle:Event')->findOneByid($id);
      $comment = new Comment();
      $commentForm = $this->getCommentForm($comment);
      $commentForm->handleRequest($request);

      if ($request->getMethod() == 'POST' and $commentForm->isValid()) {
        $comment->setRate($request->request->get('rate'));
        if ($request->getSession()->has('sessionData')) {
          $comment->setAutor($dm->getRepository('AppBundle:User')->findOneByemail(
            $request->getSession()->get('sessionData')->getEmail()
          ));
        }
        $date = new \DateTime();
        $comment->setAddTime($date);
        try{
          $item->addComment($comment);
          $dm->flush();
        }
        catch(MongoWriteConcernException $e){
          
        }
        unset($comment);
        unset($commentForm);
        $comment = new Comment();
        $commentForm = $this->getCommentForm($comment);
      }
    return $this->render('categories/event.twig',
      array('item'=>$item, 'userName'=> SessionData::sessionData($request),
        'form'=> $commentForm->createView()));

  }
    /**
     * @Route("/addevent", name="addevent");
     */
    public function addNewsAction(Request $request){
    if(!$request->getSession()->has('sessionData')){
      return $this->redirectToRoute("events");
    }
    $news = new Event();
    $form = $this->getEventsForm($news);
    $form->handleRequest($request);


    if ($form->isValid()) {
      $file = $news->getImageName();
      $fileName = md5(uniqid()).'.'.$file->guessExtension();
      $brochuresDir ='img/events';
      $file->move($brochuresDir, $fileName);
      $news->setImageName($fileName);
      $this->saveToDB($news);
      return $this->redirect('events');
    }

    return $this->render("categories/addevent.twig",
      array('form'=> $form->createView(), 'userName'=> SessionData::sessionData($request)));
  }

    public function getEventsForm($news){
    return $this->createFormBuilder($news)
      ->add('title', 'text', array('error_bubbling' => true,
        'label'=>'Title'))
      ->add('short', 'textarea', array('error_bubbling' => true,
        'label'=>'Short Description'))
      ->add('full', 'textarea', array('error_bubbling' => true,
        'label'=>'Content'))
      ->add('imageName', 'file', array('label' => 'Image'))
      ->add('save', 'submit', array('label' => 'Post'))
      ->getForm();
  }

    public function saveToDB($event) {
    $date = new \MongoDate();
      $event->setAddTime($date);
    $dm = $this->get('doctrine_mongodb')->getManager();
    $dm->persist($event);
    $dm->flush();
  }

    public function getEventsRepository() {
    return $repository = $this->get('doctrine_mongodb')
      ->getManager()
      ->getRepository('AppBundle:Event');
  }

    public function getCommentForm($comment) {
      return $this->createFormBuilder($comment)
        ->add('impressions', 'textarea', array('error_bubbling' => true,
          'label' => 'Impressions', 'attr' => array()))
        ->add('advantages', 'textarea', array('required' => false, 'error_bubbling' => true,
          'label' => 'Advantages', 'attr' => array()))
        ->add('disadvantages', 'textarea', array('required' => false, 'error_bubbling' => true,
          'label' => 'Disadvantages', 'attr' => array()))
        ->add('save', 'submit', array('label' => 'Post comment'))
        ->getForm();
    }

  }