<?php

  namespace AppBundle\Controller;
  
  use Symfony\Bundle\FrameworkBundle\Controller\Controller;
  use Symfony\Component\HttpFoundation\JsonResponse;
  use Symfony\Component\HttpFoundation\Request;
  use AppBundle\Document\User;
  use Symfony\Component\HttpFoundation\Response;
  use Symfony\Component\Form\Form;
  use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
  use Symfony\Component\Form\FormError;
  use AppBundle\SessionData;
                     use Symfony\Component\HttpFoundation\ResponseHeaderBag;
  use Symfony\Component\Serializer\Serializer;
  use Symfony\Component\Serializer\Encoder\XmlEncoder;
  use Symfony\Component\Serializer\Encoder\JsonEncoder;
  use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
  use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
  class AdminController extends Controller {

    /**
     * @Route("/admin", name="admin")
     */
    public function SigninAction(Request $request) {
      $session = $request->getSession();
      $session->clear();
      $form = $this->buildSignInForm();
      $form->handleRequest($request);
      if ($request->getMethod() == 'POST') {
        $error = $this->userInDB(array($form["nameEmail"]->getData(),
          $form["password"]->getData(), $form["superpassword"]->getData()));
        if ($error) {
          $this->redirectInTime(10);
          return $this->render('templates/message.twig', array( 'title' => 'Error!',
            'message' => $error->getMessage()));
        } else {
          $session = $request->getSession();
          $session->clear();
          $sessionData = new SessionData();
          $repository = $this->get("doctrine_mongodb")->getManager()->getRepository('AppBundle:User');
          $sessionData->setUserName($repository->findOneByemail($form["nameEmail"]->getData())->getUserName());
          $sessionData->setEmail($form["nameEmail"]->getData());
          $sessionData->setPassword(password_hash($form["password"]->getData(), PASSWORD_DEFAULT));
          $sessionData->setSuperPass(password_hash($form["superpassword"]->getData(), PASSWORD_DEFAULT));
          $session->set('sessionData', $sessionData);
          $this->redirectInTime('adminControl');
          return $this->render('templates/message.twig', array('title'=> 'You are logged in!',
            'message' => 'You are logged in with Administrator rights'));
        }
      }
      return $this->render('admin/signin.twig', array(
        'form' => $form->createView()));
    }


    /**
     * @return Form
     */
    public function buildSignInForm() {
      return $this->createFormBuilder()
        ->add('nameEmail', 'text', array('label' => 'E-Mail', 'attr' => array('align' => 'right', 'name' => 'nameEmail', 'class' => 'pure-control-group')))
        ->add('password', 'password', array('attr' => array('name' => 'password', 'class' => 'pure-control-group')))
        ->add('superpassword', 'password', array('attr' => array('name' => 'superpassword', 'class' => 'pure-control-group')))
        ->add('save', 'submit', array('label' => 'Sing In', 'attr' => array('class' => 'pure-button')))
        ->getForm();
    }


    public function userInDB($userData) {
      $repository = $this->get("doctrine_mongodb")->getManager()->getRepository('AppBundle:User');
      $user = $repository->findOneByemail($userData[0]);
      if (!$user) {
        return new FormError("The email and password that you entered don't match.");
      }
      if (!password_verify($userData[1], $user->getPassword())) {
        return new FormError("The email and password that you entered don't match.");
      }
      if (md5($userData[2]) != "a04233bce20d46857648909e6929959d") {
        return new FormError("Permission denied");
      }
      return null;
    }


    /**
     * @Route("/adminControl", name="adminControl")
     */
    public function adminControl(Request $request){
      $data = SessionData::sessionData($request);
      $error = null;
      $form = $this->buildSaveForm();
   
      if($data == null){
        $this->redirectInTime('home');
        return $this->render('templates/message.twig', array( 'title' => 'Error!',
          'message' => 'Permission denied.'));
      }

      $form->handleRequest($request);
      if($request->isMethod('POST')){
        $db = $form['Collection']->getData();
        $type = $form['Format']->getData();
        return $this->serialize($db, $type);
        
      }

      return $this->render('admin/main.twig', array('form'=>$form->createView()));

    }

    
    public function redirectInTime($url) {
      $response = new Response();
      $response->setStatusCode(200);
      $response->headers->set('Refresh', "10; url='/".$url.'"');
      $response->send();
    }
    
    /**
     * @Route("/serialize", name="serialize")
     */
    public function serialize($collectionName, $type){
      $response = new Response();
      $encoders = array(new XmlEncoder(), new JsonEncoder());
      $normalizers = array(new ObjectNormalizer());

      $serializer = new Serializer($normalizers, $encoders);

      $repository = $this->get("doctrine_mongodb")->getManager()->getRepository('AppBundle:'.$collectionName);
      $items = $repository->findAll();
      $serialized = [];
      for($i = 0; $i < count($items); $i++)
      {
        $serialized[] = $serializer->serialize($items[$i],$type);
      }

      $filename = $type.'-'.$collectionName.'.'.$type;
      file_put_contents($filename, ($serialized));

      // Generate response
      

// Set headers
      $response->headers->set('Cache-Control', 'private');
      $response->headers->set('Content-type', mime_content_type($filename));
      $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($filename) . '";');
      $response->headers->set('Content-length', filesize($filename));

// Send headers before outputting anything
      $response->sendHeaders();
      $response->setContent(file_get_contents($filename));
      return $response;
    }

    public function buildSaveForm() {
      return $this->createFormBuilder()
        ->add('Collection', 'choice', array(
          'choices' => array(
          'News' => 'News',
          'Place' => 'Place',
             'User' => 'User',
          'Event' => 'Event'
        )))
        ->add('Format', 'choice', array(
          'choices' => array(
            'xml' => 'xml',
            'json' => 'json',
          )))
        ->add('save', 'submit', array('label' => 'Export', 'attr' => array('class' => 'pure-button')))
        ->getForm();
    }
  }