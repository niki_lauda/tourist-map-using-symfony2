<?php
  namespace AppBundle\Controller;

  use Symfony\Bundle\FrameworkBundle\Controller\Controller;
  use Symfony\Component\HttpFoundation\JsonResponse;
  use Symfony\Component\HttpFoundation\Request;
  use AppBundle\Document\User;
  use Symfony\Component\HttpFoundation\Response;
  use Symfony\Component\Form\Form;
  use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
  use Symfony\Component\Form\FormError;
  use AppBundle\SessionData;

  class SignController extends Controller {
    /**
     * @Route("/registration", name="registration")
     * @param $request
     * @return Response
     */
    public function registrationAction(Request $request) {
      $user = new User();
      $form = $this->buildSignUpForm($user);
      $form->handleRequest($request);
      if ($form->isValid()) {
        if ($error = $this->userExists($user)) {
          $form->addError($error);
        } else {
          $this->saveToDB($user);
          $this->sendEmail($user);
          $this->redirectInTime(10);
          return $this->render('templates/message.twig', array('title'=> 'Thanks for registration!', 
            'message'=>'We are happy you have joined us.
    An e-mail was sent on your address. Please confirm the registration and then you will
    be able to sign-in.'));
        }
      }
      return $this->render('sign/signup.twig', array(
        'form' => $form->createView()));
    }
    
    /**
     * @Route("/signin{page}", name="signin", defaults={"page":"home"})
     * @param $request
     * @return Response
     */
    public function signInAction(Request $request, $page) {
      $form = $this->buildSignInForm();
      $form->handleRequest($request);
      if ($form->isValid()) {
        $error = $this->userInDB(array($form["nameEmail"]->getData(), 
          $password = $form["password"]->getData()));
        if ($error) {
          $form->addError($error);
        } 
        else {
          $session = $request->getSession();
          $session->clear();
          $sessionData = new SessionData();
          $repository = $this->get("doctrine_mongodb")->getManager()->getRepository('AppBundle:User');
          $sessionData->setUserName($repository->findOneByemail($form["nameEmail"]->getData())->getUserName());
          $sessionData->setEmail($form["nameEmail"]->getData());
          $sessionData->setPassword(password_hash($form["password"]->getData(), PASSWORD_DEFAULT));
          $session->set('sessionData', $sessionData);
          return $this->redirect($page); 
        }
      }
      return $this->render('sign/signin.twig', array(
        'form' => $form->createView()));
    }
    /**
     * @Route("/signout", name="signout")
     * @param $request
     * @return Response
     */
    public function signOutAction(Request $request) {
      $session = $request->getSession();
      $session->clear();
      return $this->redirectToRoute('home');
    }


    /**
     * @Route("/confirmation", name="confirmation")
     */
    public function confirmAction(Request $request){
      $userId = $request->query->get('id');
      $userHash = $request->query->get('amp;hash');
      if(!$userId or !$userHash){
        $this->redirectInTime(10);
        return $this->render("templates/message.twig", array('title'=>'Confirmation error!', 
          'message'=> 'Sorry, confirmation error has occured(retrieving get parameters)! 
          Retry registration one more time or write us an email!'));
      }
      $manager = $this->get("doctrine_mongodb")->getManager();
      $user = $manager->getRepository('AppBundle:User')->findOneByid($userId);
      if($user->getUserHash() !== $userHash){
        $this->redirectInTime(10);
        return $this->render("templates/message.twig", array('title'=>'Confirmation error!',
          'message'=> 'Sorry, confirmation error has occured(comparing hashes)! 
          Retry registration one more time or write us an email!'));
      }
      $user->setConfirmed(true);
      $manager->flush($user);
      $user = $manager->getRepository('AppBundle:User')->findOneByid($userId);
      $this->redirectInTime(10);
      return $this->render("templates/message.twig", array('title'=>'Confirmation success!',
        'message'=> 'We are glad about your confirmation! Thanks for staying with us!'));
    }
                                                                
    
    public function userInDB($userData){
      $repository = $this->get("doctrine_mongodb")->getManager()->getRepository('AppBundle:User');
      $user = $repository->findOneByemail($userData[0]);
      if(!$user){
        return new FormError("The email and password that you entered don't match.");
      }
      if (!password_verify($userData[1], $user->getPassword())) {
        return new FormError("The email and password that you entered don't match.");
      }
      return null;
    }                        
    /**
     * @param $user
     */
    public function saveToDB($user) {
      $date = new \MongoDate();
      $user->setSignUpTime($date);
      $user->setPassword(password_hash($user->getPassword(), PASSWORD_DEFAULT));
      $dm = $this->get('doctrine_mongodb')->getManager();
      $dm->persist($user);
      $dm->flush();
    }
    
    /**
     * @param $user
     * @return boolean
     */
    public function userExists($user) {
      $repository = $this->get("doctrine_mongodb")->getManager()->getRepository('AppBundle:User');
      if ($repository->findOneByuserName($user->getUserName())) {
        return new FormError("User name '" . $user->getUserName() .
          "' has been used. Enter another User name please");
      }
      if ($repository->findOneByemail($user->getEmail())) {
        return new FormError("E-mail '" . $user->getEmail() .
          "' has been used. Enter another E-mail address please");
      }
      return null;
    }

    public function redirectInTime() {
      $response = new Response();
      $response->setStatusCode(200);
      $response->headers->set('Refresh', "10; url='/'");
      $response->send();
    }

    /**
     * @param $user
     */
    public function sendEmail($user) {
      $url = $this->generateUrl('confirmation',
        array('id'=> $user->getId(),'hash' => $user->getUserHash()), true);
      $message = \Swift_Message::newInstance()
        ->setSubject('Sign Up Confirmation')
        ->setSender('touristmap2015@gmail.com')
        ->setTo($user->getEmail())
        ->setBody($this->renderView(
          'sign/email.twig', array('user'=> $user->getUserName(), 'url' => $url)
        ));
      $this->get('mailer')->send($message);
    }

    /**
     * @return Form
     */
    public function buildSignInForm() {
      return $this->createFormBuilder()
        ->add('nameEmail', 'text', array('label'=>'E-Mail', 'attr'=> array('name'=>'nameEmail')))
        ->add('password', 'password', array('attr'=> array('name'=>'password')))
        ->add('save', 'submit', array('label' => 'Sing In'))
        ->getForm();
    }
    
    /**
     * @param $user
     * @return Form
     */
    public function buildSignUpForm($user) {
      return $this->createFormBuilder($user)
        ->add('userName', 'text', array('error_bubbling' => true))
        ->add('email', 'email', array('error_bubbling' => true))
        ->add('password', 'repeated', array('type' => 'password', 'invalid_message' => 'Passwords do not match',
          'first_name' => 'password',
          'second_name' => 'confirm_password',
          'first_options' => array('label' => 'Create a Password'),
          'second_options' => array('label' => 'Confirm your Password'),
          'error_bubbling' => true))
        ->add('age', 'integer', array('required' => false, 'error_bubbling' => true))
        ->add('about', 'textarea',
          array('required' => false, 'label' => 'About me', 'max_length' => 1000, 'error_bubbling' => true))
        ->add('terms_conditions', 'checkbox',
          array('mapped' => false, 'label' => 'I agree with the terms and conditions ', 'error_bubbling' => true))
        ->add('save', 'submit', array('label' => 'Register'))
        ->getForm();
    }
  }