<?php
  namespace AppBundle;
  use Symfony\Component\HttpFoundation\Request;
  class SessionData{
    private $email;
    private $password;
    private $userName;
    private $superPass;


    /**
     * @return mixed
     */
    public function getSuperPass() {
      return $this->superPass;
    }


    /**
     * @param mixed $superPass
     * @return $this
     */
    public function setSuperPass($superPass) {
      $this->superPass = $superPass;
      return $this;
    }

    /**
     * @param Request $request
     * @return SessionData
     */
    public static function sessionData(Request $request) {
      $session = $request->getSession();
      if ($session->has('sessionData')) {
        return $session->get('sessionData');
      }
      return null;
    }

    /**
     * @return string
     */
    public function getUserName() {
      return $this->userName;
    }


    /**
     * @param string $userName
     */
    public function setUserName($userName) {
      $this->userName = $userName;
    }

    /**
     * @return string
     */
    public function getEmail() {
      return $this->email;
    }


    /**
     * @param string $email
     */
    public function setEmail($email) {
      $this->email = $email;
    }


    /**
     * @return string
     */
    public function getPassword() {
      return $this->password;
    }


    /**
     * @param mixed $password
     */
    public function setPassword($password) {
      $this->password = $password;
    }
  }