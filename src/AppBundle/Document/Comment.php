<?php

  namespace AppBundle\Document;
  use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
  use Symfony\Component\Validator\Constraints as Assert;

  /**
   * @MongoDB\EmbeddedDocument
   */
  class Comment {

    /**
     * @MongoDB\Id
     */
    private $id;
    
    /**
     * @MongoDB\Field(type="string")
     */
    private $impressions;

    /**
     * @MongoDB\Field(type="string")
     */
    private $advantages;

    /**
     * @MongoDB\Field(type="string")
     */
    private $disadvantages;

    /**
     * @MongoDB\Field(type="integer")
     */
    private $rate;

    /**
     * @MongoDB\Field(type="date")
     */
    private $addTime;

    /** @MongoDB\ReferenceOne(targetDocument="User") */
    private $autor;


    /**
     * @return mixed
     */
    public function getAutor() {
      return $this->autor;
    }


    /**
     * @param mixed $autor
     * @return $this
     */
    public function setAutor($autor) {
      $this->autor = $autor;
      return $this;
    }
    
    
    /**
     * @return mixed
     */
    public function getId() {
      return $this->id;
    }


    /**
     * @param mixed $id
     * @return $this
     */
    public function setId($id) {
      $this->id = $id;
      return $this;
    }


    /**
     * @return mixed
     */
    public function getImpressions() {
      return $this->impressions;
    }


    /**
     * @param mixed $impressions
     * @return $this
     */
    public function setImpressions($impressions) {
      $this->impressions = $impressions;
      return $this;
    }


    /**
     * @return mixed
     */
    public function getAdvantages() {
      return $this->advantages;
    }


    /**
     * @param mixed $advantages
     * @return $this
     */
    public function setAdvantages($advantages) {
      $this->advantages = $advantages;
      return $this;
    }


    /**
     * @return mixed
     */
    public function getDisadvantages() {
      return $this->disadvantages;
    }


    /**
     * @param mixed $disadvantages
     * @return $this
     */
    public function setDisadvantages($disadvantages) {
      $this->disadvantages = $disadvantages;
      return $this;
    }


    /**
     * @return mixed
     */
    public function getRate() {
      return $this->rate;
    }


    /**
     * @param mixed $rate
     * @return $this
     */
    public function setRate($rate) {
      $this->rate = $rate;
      return $this;
    }


    /**
     * @return mixed
     */
    public function getAddTime() {
      return $this->addTime;
    }


    /**
     * @param mixed $addTime
     * @return $this
     */
    public function setAddTime($addTime) {
      $this->addTime = $addTime;
      return $this;
    }


    
  }