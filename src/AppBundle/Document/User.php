<?php

namespace AppBundle\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @MongoDB\Document
 */
class User
{
    /**
     * @MongoDB\Id
     */
    protected $id;
    

    /**
     * @MongoDB\Field(type="string")
     * @Assert\Length(
     *      min = 5,
     *      max = 15,
     *      minMessage = "Your User name must be at least {{ limit }} characters long",
     *      maxMessage = "Your User name cannot be longer than {{ limit }} characters")
     */
    protected $userName;

    /**
     * @MongoDB\Field(type="string")
     * @Assert\NotBlank()
     * @Assert\Email(message="Enter valid email address please")
     */
    protected $email;

    /**
     * @MongoDB\Field(type="string")
     * @Assert\Length(
     *      min = 5,
     *      max = 20,
     *      minMessage = "Your Password must be at least {{ limit }} characters long",
     *      maxMessage = "Your Password cannot be longer than {{ limit }} characters")
     *
     */
    protected $password;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $country;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $city;

    /**
     * @MongoDB\Field(type="integer")
     * @Assert\Range(
     *      min = 6,
     *      max = 100,
     *      minMessage = "You must be at least {{ limit }} years old to enter",
     *      maxMessage = "You cannot be older than {{ limit }} years old")
     */
    protected $age;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $about;

    /**
     * @MongoDB\Field(type="date")
     */
    protected $signUpTime;
    
    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $confirmed;


    /** @MongoDB\ReferenceMany(targetDocument="Comment") */
    protected $comments;
    
    public function getUserHash(){
        return md5($this->getId() . $this->getEmail());
    }

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return $this
     */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
     * Set userName
     *
     * @param string $userName
     * @return self
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
        return $this;
    }

    /**
     * Get userName
     *
     * @return string $userName
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Get password
     *
     * @return string $password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return self
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * Get country
     *
     * @return string $country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return self
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * Get city
     *
     * @return string $city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set age
     *
     * @param integer $age
     * @return self
     */
    public function setAge($age)
    {
        $this->age = $age;
        return $this;
    }

    /**
     * Get age
     *
     * @return integer $age
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set about
     *
     * @param string $about
     * @return self
     */
    public function setAbout($about)
    {
        $this->about = $about;
        return $this;
    }

    /**
     * Get about
     *
     * @return string $about
     */
    public function getAbout()
    {
        return $this->about;
    }

    /**
     * Set signUpTime
     *
     * @param date $signUpTime
     * @return self
     */
    public function setSignUpTime($signUpTime)
    {
        $this->signUpTime = $signUpTime;
        return $this;
    }

    /**
     * Get signUpTime
     *
     * @return date $signUpTime
     */
    public function getSignUpTime()
    {
        return $this->signUpTime;
    }

    /**
     * Set confirmed
     *
     * @param boolean $confirmed
     * @return self
     */
    public function setConfirmed($confirmed)
    {
        $this->confirmed = $confirmed;
        return $this;
    }

    /**
     * Get confirmed
     *
     * @return boolean $confirmed
     */
    public function getConfirmed()
    {
        return $this->confirmed;
    }
}
