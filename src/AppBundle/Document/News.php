<?php

  namespace AppBundle\Document;
  use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
  use Symfony\Component\Validator\Constraints as Assert;

  /**
   * @MongoDB\Document
   */
  class News {
    /**
     * @MongoDB\Id
     */
    protected $id;


    /**
     * @return mixed
     */
    public function getId() {
      return $this->id;
    }


    /**
     * @param mixed $id
     * @return $this
     */
    public function setId($id) {
      $this->id = $id;
      return $this;
    }
    
    /**
     * @MongoDB\Field(type="string")
     */
    private $title;

    /**
     * @MongoDB\Field(type="string")
     */
    private $short;
    /**
     * @MongoDB\Field(type="string")
     */
    private $full;
    /**
     * @MongoDB\Field(type="string")
     */
    private $imageName;
    /**
     * @MongoDB\Field(type="date")
     */
    private $addTime;
    
    /**
     * @return mixed
     */

    /**
     * @MongoDB\EmbedMany(targetDocument="Comment")
     */
    private $comments = array();

    /**
     * @return Comment[]
     */
    public function getComments() {
      return $this->comments;
    }

    /**
     * @param Comment $comment
     * @return $this
     */
    public function addComment($comment)
    {

      $this->comments[] = $comment;
    }
    
    public function getTitle() {
      return $this->title;
    }


    /**
     * @param mixed $title
     * @return $this
     */
    public function setTitle($title) {
      $this->title = $title;
      return $this;
    }


    /**
     * @return mixed
     */
    public function getShort() {
      return $this->short;
    }


    /**
     * @param mixed $short
     * @return $this
     */
    public function setShort($short) {
      $this->short = $short;
      return $this;
    }


    /**
     * @return mixed
     */
    public function getFull() {
      return $this->full;
    }


    /**
     * @param mixed $full
     * @return $this
     */
    public function setFull($full) {
      $this->full = $full;
      return $this;
    }


    /**
     * @return mixed
     */
    public function getImageName() {
      return $this->imageName;
    }


    /**
     * @param mixed $imageName
     * @return $this
     */
    public function setImageName($imageName) {
      $this->imageName = $imageName;
      return $this;
    }


    /**
     * @return mixed
     */
    public function getAddTime() {
      return $this->addTime;
    }


    /**
     * @param mixed $addTime
     * @return $this
     */
    public function setAddTime($addTime) {
      $this->addTime = $addTime;
      return $this;
    }
  }