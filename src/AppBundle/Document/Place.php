<?php

namespace AppBundle\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Document\Comment;
/**
 * @MongoDB\Document
 */
class Place {
    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $name;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $englishName;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $description;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $fotoName;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $address;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $telephoneNumber;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $webPage;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $email;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $country;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $city;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $region;

    /**
     * @MongoDB\Field(type="float")
     */
    protected $coord_x;

    /**
     * @MongoDB\Field(type="float")
     */
    protected $coord_y;


    /**
     * @MongoDB\EmbedMany(targetDocument="Comment")
     */
    private $comments = array();
    

    /**
     * @MongoDB\Field(type="date")
     */
    protected $addTime;

    
    /**
     * @return Comment[]
     */
    public function getComments() {
        return $this->comments;
    }

    /**
     * @param Comment $comment
     * @return $this
     */
    public function addComment($comment)
    {
        if($comment == null){
            throw new \InvalidArgumentException("Null comment object is not allowed!");
        }
        $this->comments[] = $comment;
    }

    /**
     * @return mixed
     */
    public function getAddTime() {
        return $this->addTime;
    }


    /**
     * @param mixed $addTime
     * @return $this
     */
    public function setAddTime($addTime) {
        $this->addTime = $addTime;
        return $this;
    }

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set englishName
     *
     * @param string $englishName
     * @return self
     */
    public function setEnglishName($englishName)
    {
        $this->englishName = $englishName;
        return $this;
    }

    /**
     * Get englishName
     *
     * @return string $englishName
     */
    public function getEnglishName()
    {
        return $this->englishName;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set fotoName
     *
     * @param string $fotoName
     * @return self
     */
    public function setFotoName($fotoName)
    {
        $this->fotoName = $fotoName;
        return $this;
    }

    /**
     * Get fotoName
     *
     * @return string $fotoName
     */
    public function getFotoName()
    {
        return $this->fotoName;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return self
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * Get address
     *
     * @return string $address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set telephoneNumber
     *
     * @param string $telephoneNumber
     * @return self
     */
    public function setTelephoneNumber($telephoneNumber)
    {
        $this->telephoneNumber = $telephoneNumber;
        return $this;
    }

    /**
     * Get telephoneNumber
     *
     * @return string $telephoneNumber
     */
    public function getTelephoneNumber()
    {
        return $this->telephoneNumber;
    }

    /**
     * Set webPage
     *
     * @param string $webPage
     * @return self
     */
    public function setWebPage($webPage)
    {
        $this->webPage = $webPage;
        return $this;
    }

    /**
     * Get webPage
     *
     * @return string $webPage
     */
    public function getWebPage()
    {
        return $this->webPage;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return self
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * Get country
     *
     * @return string $country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return self
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * Get city
     *
     * @return string $city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set region
     *
     * @param string $region
     * @return self
     */
    public function setRegion($region)
    {
        $this->region = $region;
        return $this;
    }

    /**
     * Get region
     *
     * @return string $region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set coordX
     *
     * @param float $coordX
     * @return self
     */
    public function setCoordX($coordX)
    {
        $this->coord_x = $coordX;
        return $this;
    }

    /**
     * Get coordX
     *
     * @return float $coordX
     */
    public function getCoordX()
    {
        return $this->coord_x;
    }

    /**
     * Set coordY
     *
     * @param float $coordY
     * @return self
     */
    public function setCoordY($coordY)
    {
        $this->coord_y = $coordY;
        return $this;
    }

    /**
     * Get coordY
     *
     * @return float $coordY
     */
    public function getCoordY()
    {
        return $this->coord_y;
    }
    
    public function getShortDescription(){
        return implode('.', array_slice(explode('.', $this->description), 0, 3)).'.';
    }
}
